Read Me First

# Getting Started
###Prerequisite
Install MongoDb : https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/

Steps to install in mac

* `brew tap mongodb/brew` 
* `brew install mongodb-community@4.2`

Make sure MangoDB is up and running
To start : `brew services start mongodb-community@4.2`
