package com.apansari.webfluxexample.controller;


import com.apansari.webfluxexample.pojo.Movie;
import com.apansari.webfluxexample.pojo.MovieEvent;
import com.apansari.webfluxexample.service.WebfluxFlixService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/controller/movies")
public class WebfluxRestController {

    private final WebfluxFlixService webfluxFlixService;

    public WebfluxRestController(WebfluxFlixService webfluxFlixService) {
        this.webfluxFlixService = webfluxFlixService;
    }

    @GetMapping(value = "/{id}/events", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<MovieEvent> events(@PathVariable String id) {
        return webfluxFlixService.byId(id)
                .flatMapMany(movie -> webfluxFlixService.streamStreams(movie));
    }

    @GetMapping
    public Flux<Movie> all() {
        return webfluxFlixService.all();
    }

    @GetMapping("/{id}")
    public Mono<Movie> getById(@PathVariable String id) {
        return webfluxFlixService.byId(id);
    }
}
