package com.apansari.webfluxexample;

import com.apansari.webfluxexample.dao.MovieRepository;
import com.apansari.webfluxexample.pojo.Movie;
import com.apansari.webfluxexample.pojo.MovieEvent;
import com.apansari.webfluxexample.service.WebfluxFlixService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.reactive.function.server.support.RouterFunctionMapping;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.time.Duration;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Stream;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.ServerResponse.*;

@SpringBootApplication
public class WebfluxExampleApplication {

    @Bean
    RouterFunction<ServerResponse> route(WebfluxFlixService service) {
        return RouterFunctions.route(GET("/functional/movies"),
                request -> ok().body(service.all(), Movie.class))
                .andRoute(GET("/functional/movies/{id}"),
                        request -> ok().body(service.byId(request.pathVariable("id")), Movie.class))
                .andRoute(GET("/functional/movies/{id}/events"),
                        request -> ok().contentType(MediaType.TEXT_EVENT_STREAM)
                                .body(service.byId(request.pathVariable("id"))
                                .flatMapMany(m -> service.streamStreams(m)), MovieEvent.class));

    }

    @Bean
    CommandLineRunner demo(MovieRepository movieRepository) {
        return args -> {

            movieRepository
                    .deleteAll()
                    .subscribe(null, null, () -> Stream.of("Aeon Flux", "Enter the Mono<Void>", "The Fluxinator",
                            "Silence of the Lambdas", "Reactive Mongos on Plane",
                            "Back to the Future", "Attack of the Fluxxes", "Y Tu Mono Tambien")
                            .map(name -> new Movie(UUID.randomUUID().toString(), name, randomGenre()))
                            .forEach(movie -> movieRepository.save(movie).subscribe(System.out::println)));
        };
    }


    private String randomGenre() {
        String[] genre = "horror, romcom, drama, action, documentry".split(",");
        return genre[new Random().nextInt(genre.length)];
    }


    public static void main(String[] args) {
        SpringApplication.run(WebfluxExampleApplication.class, args);
    }
}
