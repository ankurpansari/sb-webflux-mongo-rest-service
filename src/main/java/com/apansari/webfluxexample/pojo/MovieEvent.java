package com.apansari.webfluxexample.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class MovieEvent {
    private Movie movie;
    private Date when;
    private String user;
}
