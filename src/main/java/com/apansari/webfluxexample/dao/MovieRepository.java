package com.apansari.webfluxexample.dao;

import com.apansari.webfluxexample.pojo.Movie;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface MovieRepository extends ReactiveMongoRepository<Movie, String> {
}
